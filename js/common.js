$(document).ready(function() {

	// MENU oil_dc / cht_egt
	$('#cht_egt_click').on('click touchstart',function() {
		$(this).addClass('active');
		$('#oil_dc_click, #flaps_click').removeClass('active');
		$('#oil_dc, #flaps').hide();
		$('#cht_egt').show();
	});
	$('#oil_dc_click').on('click touchstart',function() {
		$(this).addClass('active');
		$('#cht_egt_click, #flaps_click').removeClass('active');
		$('#cht_egt, #flaps').hide();
		$('#oil_dc').show();
	});
	$('#flaps_click').on('click touchstart',function() {
		$(this).addClass('active');
		$('#cht_egt_click, #oil_dc_click').removeClass('active');
		$('#cht_egt, #oil_dc').hide();
		$('#flaps').show();
	});


	// to_main2
	$('#to_main2').click(function() {
		$('.mainscreen1').hide();
		$('.mainscreen2').show();
		return false;
	});
	$('#to_main3').click(function() {
		$('.mainscreen2').hide();
		$('.mainscreen3').show();
		return false;
	});
	$('#to_main1').click(function() {
		$('.mainscreen3').hide();
		$('.mainscreen1').show();
		return false;
	});




	// js_settings
	$('.js_settings').click(function() {
		$('.settings').toggleClass('open');
		return false;
	});
	$('.js_settings2').click(function() {
		$('.settings2').toggleClass('open');
		return false;
	});




	// Colors
	var red = '#ff0000';
	var yellow = '#ffe500';
	var green = '#1cdb1c';


	// RPM
	var handle = $( "#custom-handle" );
	$( "#slider" ).slider({
		min: 0,
		max: 300,
		create: function() {
			handle.text( $( this ).slider( "value" ) );
		},
		slide: function( event, ui ) {
			handle.text( ui.value );

			$('.bentscale.mirror .bentscale_range').css({'height': ui.value});
			$('.bentscale.mirror .bentscale_value').text(ui.value);

			if ( ui.value > 44 && ui.value < 209 ) {
				$('.bentscale.mirror .bentscale_range_svg path').attr({'fill': green});
			} else if ( ui.value > 208 && ui.value < 250 ) {
				$('.bentscale.mirror .bentscale_range_svg path').attr({'fill': yellow});
			}
			 else if ( ui.value > 249 ) {
				$('.bentscale.mirror .bentscale_range_svg path').attr({'fill': red});
			} else {
				$('.bentscale.mirror .bentscale_range_svg path').attr({'fill': '#828282'});
			}
		}
    });

	// TANK R
    var handle2 = $( "#custom-handle2" );
	$( "#slider2" ).slider({
		min: 0,
		max: 217,
		create: function() {
			handle2.text( $( this ).slider( "value" ) );
		},
		slide: function( event, ui ) {
			handle2.text( ui.value );

			$('#scale_range').css({'height': ui.value});
			$('#scale_val').text(ui.value);

			if ( ui.value > 19 && ui.value < 37 ) {
				$('#scale_range').css({'background': yellow});
			} else if ( ui.value > 36 && ui.value < 199  ) {
				$('#scale_range').css({'background': green});
			} else if ( ui.value > 198 ) {
				$('#scale_range').css({'background': red});
			} else {
				$('#scale_range').css({'background': red});
			}
		}
    });


	// TRIM
    var handle3 = $( "#custom-handle3" );
	$( "#slider3" ).slider({
		min: 0,
		max: 40,
		value: 20,
		slide: function( event, ui ) {
			var trim_value = ui.value;

			if ( trim_value > 20 ) {
				trim_value = trim_value - 20;
				$('.trimbox_arrows').css({'transform': 'translateY(-'+(trim_value * 1.7)+'px)'});
				$('#trim_val').text(trim_value);
				handle3.text(trim_value);
			} else {
				trim_value = 20 - trim_value;
				$('.trimbox_arrows').css({'transform': 'translateY('+(trim_value * 1.7)+'px)'});
				$('#trim_val').text('-'+trim_value);
				handle3.text('-'+trim_value);
			}

			if ( trim_value > 10 ) {
				$('.trimbox_arrow').addClass('yellow');
				$('#trim_val, #trim_smallname').addClass('yellow');
			} else {
				$('.trimbox_arrow').removeClass('yellow');
				$('#trim_val, #trim_smallname').removeClass('yellow');
			}
		}
    });


    // TRIM
    var handle4 = $( "#custom-handle4" );
	$( "#slider4" ).slider({
		min: 0,
		max: 40,
		value: 20,
		slide: function( event, ui ) {

			var flaps_value = ui.value;
			if ( flaps_value > 20 ) {
				flaps_value = flaps_value - 20;
				$('.flapsbox_arrow').css({'transform': 'rotate(-'+(flaps_value*2.5)+'deg)'});
				$('#flapsbox_val, #flapsbox_val_small').text('-'+flaps_value);
				handle4.text('-'+flaps_value);
			} else {
				flaps_value = 20 - flaps_value;
				$('.flapsbox_arrow').css({'transform': 'rotate('+(flaps_value*2.5)+'deg)'});
				$('#flapsbox_val, #flapsbox_val_small').text(flaps_value);
				handle4.text(flaps_value);
			}

			if ( ui.value > 20 ) {
	    		$('.flapsbox_arrow').addClass('grey');
	    		$('.flapsbox_value').css({'color': '#828282'});
	    		$('.flapsbox_val_color').css({'color': '#828282'});
	    	} else if ( ui.value > 10 && ui.value < 21 ) {
	    		$('.flapsbox_arrow').removeClass('grey');
	    		$('.flapsbox_arrow').removeClass('yel');
	    		$('.flapsbox_value').css({'color': green});
	    		$('.flapsbox_val_color').css({'color': green});
	    	} else {
	    		$('.flapsbox_arrow').removeClass('grey');
	    		$('.flapsbox_arrow').addClass('yel');
	    		$('.flapsbox_value').css({'color': yellow});
	    		$('.flapsbox_val_color').css({'color': yellow});
	    	}
		}
    });


    // TRIM top scale
    var handle5 = $( "#custom-handle5" );
	$( "#slider5" ).slider({
		min: 0,
		max: 40,
		value: 20,
		slide: function( event, ui ) {
			handle5.text( ui.value );
			var topscale_val = ui.value;
			
			if ( ui.value > 20 ) {
				topscale_val = topscale_val - 20;
				topscale_val = topscale_val * 2.35;
				$('.trimbox_topscale_left').css({'width': 0});
				$('.trimbox_topscale_right').css({'width': topscale_val+'px'});
			} else {
				topscale_val = 20 - topscale_val;
				topscale_val = topscale_val * 2.35;
				$('.trimbox_topscale_right').css({'width': 0});
				$('.trimbox_topscale_left').css({'width': topscale_val+'px'});
			}

			if ( $('.trimbox_topscale_left').width() > 18 || $('.trimbox_topscale_right').width() > 19 ) {
				$('.trimbox_topscale svg circle').attr({'stroke': yellow});
			} else {
				$('.trimbox_topscale svg circle').attr({'stroke': green});
			}
		}
    });


    // TRIM bottom scale
    var handle6 = $( "#custom-handle6" );
	$( "#slider-trim-bottom" ).slider({
		min: 0,
		max: 40,
		value: 20,
		slide: function( event, ui ) {
			handle6.text( ui.value );
			var botscale_val = ui.value;
			
			if ( ui.value > 20 ) {
				botscale_val = botscale_val - 20;
				botscale_val = botscale_val * 2.1;
				$('.trimbox_bottom_left').css({'width': 0});
				$('.trimbox_bottom_right').css({'width': botscale_val+'px'});
			} else {
				botscale_val = 20 - botscale_val;
				botscale_val = botscale_val * 2.1;
				$('.trimbox_bottom_right').css({'width': 0});
				$('.trimbox_bottom_left').css({'width': botscale_val+'px'});
			}

			if ( $('.trimbox_bottom_left').width() > 21 || $('.trimbox_bottom_right').width() > 21 ) {
				$('.trimbox_bottom_range').css({'background': yellow});
			} else {
				$('.trimbox_bottom_range').css({'background': green});
			}
		}
    });


    // GEAR / CANOPY
    $('.js_gear').click(function() {
    	$(this).toggleClass('active');
    	$('.smallwidjet_name_gear').toggleClass('red');
    	$('#gear_signal').toggleClass('active');
    	return false;
    });
    $('.js_canopy').click(function() {
    	$(this).toggleClass('active');
    	$('.smallwidjet_name_canopy').toggleClass('red');
    	$('#canopy_signal').toggleClass('active');
    	return false;
    });


    // Compas
    let compasHandle = $('#custom-handle7');
	$('#compas_slider').slider({
		min: 0,
		max: 360,
		value: 0,
		slide: function(event,ui) {
			compasHandle.text(ui.value);
			let compasbox = $('#compasbox');
			compasbox.css({'transform': 'rotate(-' + ui.value + 'deg)'});
			$('.compas_dig').css({'transform': 'rotate(' + ui.value + 'deg)'});
		}
    });

    // Compas arrow
    let compasHandle2 = $('#custom-handle8');
	$('#compas_slider_arrow').slider({
		min: 0,
		max: 360,
		value: 0,
		slide: function(event,ui) {
			compasHandle2.text(ui.value);
			let compasArrow = $('#compas_arrow');
			$('.course_value').text(ui.value);
			compasArrow.css({'transform': 'rotate(' + ui.value + 'deg)'});
		}
    });

    // Course buttons
    let course_count = 0;
    let timeout = 0;
    $('#course_left').on('mousedown',function() {
    	timeout = setInterval(function(){
    		//if ( course_count > 0 ) {
    			course_count--;
    		//}
    		let positive = course_count < 0 ? -course_count : course_count;
    		$('#compas_arrow').css({'transform': 'rotate(' + course_count + 'deg)'});
    		$('.course_value').text(positive%360);
    	},10);
    });
    $('#course_right').on('mousedown',function() {
    	timeout = setInterval(function(){
    		//if ( course_count < 360 ) {
    			course_count++;
    		//}
    		let positive = course_count < 0 ? -course_count : course_count;
    		$('#compas_arrow').css({'transform': 'rotate(' + course_count + 'deg)'});
    		$('.course_value').text(positive%360);
    	},10);
    });
    
    $('#course_left, #course_right').on('mouseup',function() {
    	clearInterval(timeout);
    });



    // Compas line
    let compasHandle3 = $('#custom-handle9');
	$('#compas_slider_line').slider({
		min: 0,
		max: 60,
		value: 30,
		slide: function(event,ui) {
			let compasLine = $('.compas_arrow_line');
			if ( ui.value > 30 ) {
				ui.value = ui.value - 30;
				let dig = ui.value/12;
				$('.deviation_value').text(dig.toFixed(2));
				compasLine.css({'transform': 'translateX('+ui.value+'px)'});
				compasHandle3.text(ui.value);
				$('.deviation_arrow').text('>');
			} else {
				ui.value = 30 - ui.value;
				let dig = ui.value/12;
				$('.deviation_value').text(dig.toFixed(2));
				compasLine.css({'transform': 'translateX(-'+ui.value+'px)'});
				compasHandle3.text('-'+ui.value);
				$('.deviation_arrow').text('<');
			}
		}
    });

    // wind_meter_slide
    let compasHandle10 = $('#custom-handle10');
	$('#wind_meter_slide').slider({
		min: 0,
		max: 360,
		value: 0,
		slide: function(event,ui) {
			let windArrow = $('.wind_meter_arrow');
			windArrow.css({'transform': 'rotate(' + ui.value + 'deg)'});
			$('.wind_value').text(ui.value);
		}
    });

    // compas_scale
    var compasScaleSlide = $( "#custom-handle11" );
	$( "#compas_scale_slide" ).slider({
		min: 0,
		max: 80,
		value: 40,
		slide: function(event,ui) {
			compasScaleSlide.text(ui.value);
			if ( ui.value > 40 ) {
				ui.value = ui.value - 40;
				$('.compas_scale_left').css({'width': 0});
				$('.compas_scale_right').css({'width': ui.value+'px'});
				compasScaleSlide.text('+'+ui.value);
			} else {
				ui.value = 40 - ui.value;
				$('.compas_scale_right').css({'width': 0});
				$('.compas_scale_left').css({'width': ui.value+'px'});
				compasScaleSlide.text('-'+ui.value);
			}
		}
    });


    // Gyroball
    var gyroSlide = $('#handle-gyro');
	$('#slider-gyro').slider({
		min: 0,
		max: 20,
		value: 10,
		slide: function(event,ui) {
			gyroSlide.text(ui.value);
			if ( ui.value > 10 ) {
				ui.value = ui.value - 10;
				let gyronum = ui.value * 5;
				$('.gyroball_dot').css({'transform': 'translateX(' + gyronum + 'px)'});
			} else {
				ui.value = 10 - ui.value;
				let gyronum = ui.value * 5;
				$('.gyroball_dot').css({'transform': 'translateX(-' + gyronum + 'px)'});
			}
		}
    });


    // custom-handle-kmh
    var kmhSlide = $('#handle-kmh');
	$('#slider-kmh').slider({
		min: 0,
		max: 300,
		value: 0,
		slide: function(event,ui) {
			kmhSlide.text(ui.value);
			let num = 3 * ui.value;
			$('#numbers1 .numbers_scale').css({'transform': 'translateY(' + num + 'px)'});
			$('#numbers1 .numbers_fixed_num').text(ui.value);
			$('#numbers1 .numbers_fixed_slide_top b').text(ui.value + 1);
			$('#numbers1 .numbers_fixed_slide_bottom b').text(ui.value - 1);

			if ( $('#numbers1 .numbers_fixed_num').text() == 0 ) {
				$('#numbers1 .numbers_fixed_slide_bottom').hide();
			} else if ( $('#numbers1 .numbers_fixed_num').text() == 300 ) {
				$('#numbers1 .numbers_fixed_slide_top').hide();
			} else {
				$('#numbers1 .numbers_fixed_slide_bottom').show();
				$('#numbers1 .numbers_fixed_slide_top').show();
			}
		}
    });


	// custom-handle-m
    var mSlide = $('#handle-m');
	$('#slider-m').slider({
		min: 0,
		max: 340,
		value: 0,
		slide: function(event,ui) {
			mSlide.text(ui.value);
			let num = 3 * ui.value;
			$('#numbers2 .numbers_scale').css({'transform': 'translateY(' + num + 'px)'});
			let sum = (ui.value*10) + 2400;
			$('#numbers2 .numbers_fixed_num').text(sum);
			$('#numbers2 .numbers_fixed_slide_top b').text(sum + 10);
			$('#numbers2 .numbers_fixed_slide_bottom b').text(sum - 10);

			if ( $('#numbers2 .numbers_fixed_num').text() == 0 ) {
				$('#numbers2 .numbers_fixed_slide_bottom').hide();
			} else if ( $('#numbers2 .numbers_fixed_num').text() == 300 ) {
				$('#numbers2 .numbers_fixed_slide_top').hide();
			} else {
				$('#numbers2 .numbers_fixed_slide_bottom').show();
				$('#numbers2 .numbers_fixed_slide_top').show();
			}
		}
    });


    // compastop_scale
    var compasSlide = $('#handle-compas');
	$('#slider-compas').slider({
		min: 0,
		max: 720,
		value: 360,
		slide: function(event,ui) {
			//compasSlide.text(ui.value);
			if ( ui.value > 360 ) {
				let num2 = (ui.value - 360) * 6;
				$('.compastop_scale').css({'transform': 'translateX(-' + num2 + 'px)'});
				$('.compastop_arrow').text(ui.value - 360);
			} else {
				let num3 = (360 - ui.value) * 6;
				$('.compastop_scale').css({'transform': 'translateX(' + num3 + 'px)'});
				$('.compastop_arrow').text(ui.value);
			}
			switch(ui.value) {
				case 360:
				case 720:
					$('.compastop_arrow').text('0');
					break;
			}
		}
    });



    // SCREEN 2 plane
    var screen2Slide = $('#handle-screen2');
	$('#slider-screen2').slider({
		min: 0,
		max: 30,
		value: 15,
		slide: function(event,ui) {
			screen2Slide.text(ui.value);
			if ( ui.value > 15 ) {
				ui.value = ui.value - 15;
				let scr2 = ui.value * 3.1;
				$('.mainscreen2_plane').css({'transform': 'rotate(' + scr2 + 'deg)'});
			} else {
				ui.value = 15 - ui.value;
				let scr2 = ui.value * 3.1;
				$('.mainscreen2_plane').css({'transform': 'rotate(-' + scr2 + 'deg)'});
			}
		}
    });
    // slider-pitch
    var pitchSlide = $('#handle-pitch');
	$('#slider-pitch').slider({
		min: 0,
		max: 100,
		value: 50,
		slide: function(event,ui) {
			pitchSlide.text(ui.value);
			if ( ui.value > 50 ) {
				ui.value = ui.value - 50;
				let pitch = ui.value * 23.05;
				$('.mainscreen2 .mainscreen2_bg').css({'transform': 'translateY(-' + pitch + 'px)'});
				$('.mainscreen2 .pitch_scale_inner').css({'transform': 'translateY(-' + pitch + 'px)'});
			} else {
				ui.value = 50 - ui.value;
				let pitch = ui.value * 23.05;
				$('.mainscreen2 .mainscreen2_bg').css({'transform': 'translateY(' + pitch + 'px)'});
				$('.mainscreen2 .pitch_scale_inner').css({'transform': 'translateY(' + pitch + 'px)'});
			}
		}
    });



    // SCREEN 3 plane
    var screen3Slide = $('#handle-screen3');
	$('#slider-screen3').slider({
		min: 0,
		max: 30,
		value: 15,
		slide: function(event,ui) {
			screen3Slide.text(ui.value);
			if ( ui.value > 15 ) {
				ui.value = ui.value - 15;
				let lean = ui.value * 3.5;
				let lean2 = ui.value * 3.1;
				$('.plane1_arrow').css({'transform': 'rotate(' + lean + 'deg)'});
				$('.plane2_arrow').css({'transform': 'rotate(' + lean2 + 'deg)'});
			} else {
				ui.value = 15 - ui.value;
				let lean = ui.value * 3.7;
				let lean2 = ui.value * 3.1;
				$('.plane1_arrow').css({'transform': 'rotate(-' + lean + 'deg)'});
				$('.plane2_arrow').css({'transform': 'rotate(-' + lean2 + 'deg)'});
			}
		}
    });


	// Screen1 heeling
    var heelingSlide = $('#handle-heeling');
	$('#screen3-heeling').slider({
		min: 0,
		max: 30,
		value: 15,
		slide: function(event,ui) {
			heelingSlide.text(ui.value);
			if ( ui.value > 15 ) {
				ui.value = ui.value - 15;
				let pitch = ui.value * 3.05;
				$('.mainscreen1 .mainscreen2_bg_wrap').css({'transform': 'rotate(-' + pitch + 'deg)'});
			} else {
				ui.value = 15 - ui.value;
				let pitch = ui.value * 3.05;
				$('.mainscreen1 .mainscreen2_bg_wrap').css({'transform': 'rotate(' + pitch + 'deg)'});
			}
		}
    });
    var pitch2Slide = $('#handle-pitch2');
	$('#screen3-pitch').slider({
		min: 0,
		max: 100,
		value: 50,
		slide: function(event,ui) {
			pitch2Slide.text(ui.value);
			if ( ui.value > 50 ) {
				ui.value = ui.value - 50;
				let pitch = ui.value * 23.05;
				$('.mainscreen1 .mainscreen2_bg').css({'transform': 'translateY(-' + pitch + 'px)'});
				$('.mainscreen1 .pitch_scale_inner').css({'transform': 'translateY(-' + pitch + 'px)'});
			} else {
				ui.value = 50 - ui.value;
				let pitch = ui.value * 23.05;
				$('.mainscreen1 .mainscreen2_bg').css({'transform': 'translateY(' + pitch + 'px)'});
				$('.mainscreen1 .pitch_scale_inner').css({'transform': 'translateY(' + pitch + 'px)'});
			}
		}
    });


    // Angle
    var angleSlide = $('#handle-angle');
	$('#angle-slider').slider({
		min: 0,
		max: 40,
		value: 20,
		slide: function(event,ui) {
			angleSlide.text(ui.value);
			let angleDig = ui.value * 1.325;

			if ( ui.value < 20 ) {
				$('.angle_range_bottom').css({'height': 26 - angleDig, 'display': 'block'});
				$('.angle_range_top').css({'display': 'none'});
			} else {
				$('.angle_range_top').css({'height': angleDig - 26, 'display': 'block'});
				$('.angle_range_bottom').css({'display': 'none'});
			}
			
			if ( ui.value > 7 && ui.value < 15 ) {
				$('.angle_range svg ellipse').css({'stroke': yellow});
			} else if ( ui.value > 14 && ui.value < 26 ) {
				$('.angle_range svg ellipse').css({'stroke': green});
			} else if ( ui.value > 25 && ui.value < 35 ) {
				$('.angle_range svg ellipse').css({'stroke': yellow});
			} else {
				$('.angle_range svg ellipse').css({'stroke': red});
			}
		}
    });


    // Speed height
    var heightSlide = $('#handle-height');
	$('#height-slider').slider({
		min: 0,
		max: 100,
		value: 50,
		slide: function(event,ui) {
			//heightSlide.text(ui.value);
			$('.speed_height_arrow').text(ui.value);

			if ( ui.value > 50 ) {
				let height = ui.value - 50;
				let heightNew = height * 2.53;
				$('.speed_height_arrow').css({'transform': 'translateY(-' + heightNew + 'px)'});
			} else {
				let height = 50 - ui.value;
				let heightNew = height * 2.53;
				$('.speed_height_arrow').css({'transform': 'translateY(' + heightNew + 'px)'});
			}

		}
    });


});